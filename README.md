		malmon - MALware MONitor

------
About:
------
malmon is a small peace of software witch is very useful
for web hosting server (and not only).
It uses inotify (pyinotify) to monitor file system activity
and checks every new / movied / modified file (there's a file
size limit , so we can check only files smaller then X size).
Currently there two type of checks:

    # md5 sum check.
		* We have a db of md5 sums of some known
		  malware/exploits/backdoors and stuff like that
		  and we compare them to the md5 sum of the new/
		  modified file.
    # hex signature check.
		* We check the file content and match it
		  agains another db with known hex signatures.

---------
Features:
---------
    1). Realtime file system multi threaded monitoring
    2). md5 sum checks
    3). hex signature based checks
    4). exclude file (supports reg expr)
    5). Auto update [md5sums & hex sigs]
    6). Check only files smaller them some
	size (size limit is set in the config)
    7). Shitlist added. A list of forbiden filenames
	This will move all files with name found in
	the shitlist, without doing any checks
    8). Add warn/paranoid mode option
	    In paranoid mode (default) we mode all
	    infections to the quarantine dir
	    In warn mode, we will only print them
	    in the log/stdout, without moving them
    9). Lower process priorit (lower resurse usage)
    10). Add/Remove directories in watch list while
	    running (throu the UNIX socket)
    11). Force update (throu the UNIX socket)
    12). New option - scan directories for malware

---------------
What's to come:
---------------
    1). Add MIME Type check.
    2). Add spam-filter like checks.
	    We will analyze the file, and match
	    any strings that are often found in
	    malware/backdoors/exploits.
	    Every sting will have factor, and
	    after the analyze is done, we check
	    what factor has the file get, and if
	    its bigger then allowed we'll add
	    warning to the log/stdout (depending
	    in what mode is the daemon running)
    3). Mail reports (daily and on event)

-------
Layout:
-------
	/usr/local/sbin/malmon       - The daemon
	/usr/local/sbin/malmon-scan  - Scanning tool
	/etc/malmon                  - Configuration dir
	/etc/malmon/malmon.conf      - Config file
	/etc/malmon/black.list       - Backlist file (supports reg expr)
	/etc/malmon/exclude.list     - Exclude list (supports reg expr)
	/var/cache/malmon            - The daemon cache directory
	/var/cache/malmon/infected   - Where to move all infected files
	/var/run/malmon.pid          - The pidfile
	/var/run/malmon.sock         - Unix socket file

------------
Known issues
------------
If you get WD=-1 No space left on device (ENOSPC)
this means you've reached the kernel fs.inotify.max_user_watches limit

	sysctl -n fs.inotify.max_user_watches		# to read it
	sysctl -n -w fs.inotify.max_user_watches=16384	# to modify it



-----
Deps:
-----
	# inotify kernel support	(CONFIG_INOTIFY_USER=y)
	# pyinotify module		(http://pyinotify.sourceforge.net/)
If you have them both, you're good to go :]


#Small tip:
	hashlib became part of Python in version 2.6
		if you're using older version, you'll need to
		get hashlib for python2.4+ from python.org.
	malmon was only tested with Python2.5...so
		no clue if it will work with 2.4 (it should!)
