#!/bin/bash

# malmon install script
conf_files="exclude.list malmon.conf black.list"

die() {
    echo $1
    exit 1
    }

# Some stupid check :]
if ! [ -e ./src/malmon.py ]; then
	echo "./src/malmon.py missing!"
	exit 1
fi

# uid == 0 ?
if [ "$(id -u)" != "0" ]; then
	die "This script must be run as root" 1>&2
fi

# Check for instaleld modules. (pyinotify)
python -c "import pyinotify" &>/dev/null \
	|| die "Unable to find pyintofy on the system.\nGet it from: http://pyinotify.sourceforge.net/"


# Move the daemon.
echo -n "Installing malmon into /usr/local/sbin/ "
cp -f ./src/malmon.py /usr/local/sbin/malmon \
	|| die "Error while moving malmon.py to /usr/local/sbin"
cp -f ./src/malmon-scan.py /usr/local/sbin/malmon-scan \
	|| die "Error while moving malmon-scan.py to /usr/local/sbin"
cp -f ./src/malmon-ctl.py /usr/local/sbin/malmon-ctl \
	|| die "Error while moving malmon-ctl.py to /usr/local/sbin"
chmod +x /usr/local/sbin/malmon
chmod +x /usr/local/sbin/malmon-scan
chmod +x /usr/local/sbin/malmon-ctl
echo "Done."


# Create cache dir
echo -n "Creating cache dirs... "
if ! [ -d /var/cache/malmon ]; then
	mkdir /var/cache/malmon || die "Error while creating /var/cache/malmon"
	mkdir /var/cache/malmon/infected \
		|| die "Error while creating /var/cache/malmon/infections"
fi
echo "Done"


# Create conf dir
echo -n "Creating configration... "
if ! [ -d /etc/malmon ]; then
	mkdir /etc/malmon || \
		die "Error while createing /etc/malmon"
fi
# Copy files
for filename in $conf_files; do
	if [ -e "/etc/malmon/$filename" ]; then
		echo "/etc/malmon/$filename already exists. Using .new"
		filename2="$filename.new"
	fi
	cp -f "./src/conf/$filename" /etc/malmon/$filename2 \
		|| die "Error while coping $filename"
done

echo "All done."
echo; echo
echo "Edit the config file /etc/malmon/malmon.conf"
echo "Run the daemon: /usr/local/sbin/malmon"
echo
echo "sysctl -n -w fs.inotify.max_user_watches=16384"
echo "To modify max watches value"
echo

exit 0
