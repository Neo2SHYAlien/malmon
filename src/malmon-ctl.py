#!/usr/bin/env python
#
#     Name:          malmon-ctl
#     Version:       0.3.99d3
#     Authors:       l4m3rx & Neo2SHYAlien
#     Contact:       <shadowx-at-escom.bg>
#                    <neo2shyalien-at-neo2shyalien.eu>

__version__	= '0.3.99d3'

import os
from optparse import OptionParser
from ConfigParser import ConfigParser

#lets add some options
parser = OptionParser()
parser.add_option("-a", "--add", dest="add",
                  help="Add directory for monitoring", metavar="PATH")
parser.add_option("-s", "--scan", dest="scan",
                  help="Scan directory for malshit", metavar="PATH")
parser.add_option("-d", "--date", dest="date",
                  help="Print log for certain date", metavar="YYYY-MM-DD")
parser.add_option("-f", "--filter", dest="filter",
                  help="Print log for certain word", metavar="WORD")
parser.add_option("-l", "--log",
				action="store_true", dest="log", default=False,
				help="Print last 20 lines from malmon log")
parser.add_option("-q", "--quiet",
				action="store_false", dest="verbose", default=True,
				help="don't print status messages to stdout only mail report")
parser.add_option("-r", "--restart",
				action="store_true", dest="restart", default=False,
				help="Restart malmon process")
parser.add_option("-u", "--update",
				action="store_true", dest="restart", default=False,
				help="Force malmon update")
parser.add_option("-v", "--version",
				action="store_true", dest="version", default=False,
				help="Print malmon version")

(options, args) = parser.parse_args()

#print malmon version
if options.version:
	print "malmon version %s" % __version__

#append new dir for monitoring
if options.add:
	print "Append for monitoring %s" % options.add

#run scanning
if options.scan:
	print "Run scanning for %s" % options.scan
	os.system("malmon-scan %s" % options.scan)

#restart
if options.restart:
	print "Malmon restartg. Actually this is default behavior on malmon demon start"
	os.system("malmon")

#force update - just restart the malmon	
if options.update:
	print "Malmon update. Actually this is default behavior on malmon demon start"
	os.system("malmon")

# Read conf
config = ConfigParser()
config.read(conf)
logfile = config.get("logging", "logfile")

#log print
if options.log:
	os.system("tail -n 20 %s" % logfile)
elif options.filter:
	os.system("grep '%s' %s" % (options.filter, logfile))
elif options.date:
	os.system("grep '%s' %s" % (options.date, logfile))
	
exit()
