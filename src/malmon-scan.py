#!/usr/bin/env python

conf = '/etc/malmon/malmon.conf'

import socket
import os
from os import popen, path
from sys import argv, exit
from ConfigParser import ConfigParser

def usage():
	print 'Usage:'
	print '\t', argv[0], '<arguments> <path>'
	print '\t\targuments\t- find syntax'
	print '\t\tpath\t\t- path to the dir you want to scan'
	print

def send(msg, sockfile):
	msg = "f:" + msg
	sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	sock.connect(sockfile)
	sock.send(msg)
	sock.close()

def get_filepaths(directory):
	file_paths = []  # List which will store all of the full filepaths.

	# Walk the tree.
	for root, directories, files in os.walk(directory):

		for filename in files:

			# Join the two strings in order to form the full filepath.
			filepath = os.path.join(root, filename)

			file_paths.append(filepath)  # Add it to the list.

	return file_paths  # Self-explanatory.

# Check args
if len(argv) == 1:
	usage()
	exit(0)

# Read conf
config = ConfigParser()
config.read(conf)
sockfile = config.get("global", "sockfile")

if not path.exists(sockfile):
	print "malmon is not running."
	print"malmon must be running to use this option."
	exit()

result = get_filepaths(argv[1])

for file in result:
	if file:
		send(file, sockfile)
		print "Adding:", file

print 'Finish!'
exit()
